//
//  TableTests.swift
//  ToyRobotTests
//
//  Created by JoGoFo on 9/7/19.
//  Copyright © 2019 JOGOFO Pty Ltd. All rights reserved.
//

import XCTest
@testable import ToyRobot

class TableTests: XCTestCase {

    func testPlaceInBounds() {
        let table = Table(width: 5, length: 5)
        
        try! table.place(StubMovableObject(), position: Position(x: 0, y: 0))
        XCTAssertTrue(table.objects.count == 1)
        
        try! table.place(StubMovableObject(), position: Position(x: 4, y: 4))
        XCTAssertTrue(table.objects.count == 2)
        
        XCTAssertThrowsError(try table.place(StubMovableObject(), position: Position(x: 5, y: 0))) { error in
            XCTAssertTrue(table.objects.count == 2)
        }
        
        XCTAssertThrowsError(try table.place(StubMovableObject(), position: Position(x: 0, y: 5))) { error in
            XCTAssertTrue(table.objects.count == 2)
        }
    }
    
    func testMove() {
        let table = Table(width: 5, length: 5)
        
        let stub1 = StubMovableObject()
        let pos1 = Position(x: 0, y: 0)
        try! table.place(stub1, position: pos1)
        stub1.position = pos1
        let newPos1 = try! table.positionAfterMoving(stub1, spaces: 1, towards: .north)
        XCTAssertEqual(newPos1, Position(x: 0, y: 1))
        
        XCTAssertThrowsError(try table.positionAfterMoving(stub1, spaces: 1, towards: .west)) { error in
            XCTAssertEqual(PositionError.outOfBounds, error as! PositionError)
        }
        
        let stub2 = StubMovableObject()
        let pos2 = Position(x: 4, y: 0)
        try! table.place(stub2, position: pos2)
        stub2.position = pos2
        XCTAssertThrowsError(try table.positionAfterMoving(stub2, spaces: 1, towards: .east)) { error in
            XCTAssertEqual(PositionError.outOfBounds, error as! PositionError)
        }
    }

}

private class StubMovableObject: MovableObject {
    var position = Position.invalidPosition
}
