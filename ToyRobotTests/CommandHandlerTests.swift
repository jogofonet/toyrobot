//
//  CommandHandlerTests.swift
//  ToyRobotTests
//
//  Created by JoGoFo on 10/7/19.
//  Copyright © 2019 JOGOFO Pty Ltd. All rights reserved.
//

import XCTest
@testable import ToyRobot

class CommandHandlerTests: XCTestCase {
    
    var coordinator: ToyRobotCoordinator!
    var commandInterpreter: CommandInterpreter!
    var commandHandler: CommandHandler!
    
    override func setUp() {
        coordinator = ToyRobotCoordinator()
        commandInterpreter = CommandInterpreter()
        commandHandler = CommandHandler(coordinator: coordinator)
    }

    /**
         PLACE 0,0,NORTH
         MOVE
         REPORT
         Output: 0,1,NORTH
     */
    func testProvidedScenarioA() {
        let commands = """
            PLACE 0,0,NORTH
            MOVE
            REPORT
            """
        let outputArray = commands.components(separatedBy: .newlines)
            .map { try! commandInterpreter.interpret(command: $0) }
            .map { try? commandHandler.handle(command: $0) }
        XCTAssertEqual(outputArray.last, "0,1,NORTH")
    }
    
    
    
    /**
         PLACE 0,0,NORTH
         LEFT
         REPORT
         Output: 0,0,WEST
     */
    func testProvidedScenarioB() {
        let commands = """
            PLACE 0,0,NORTH
            LEFT
            REPORT
            """
        let outputArray = commands.components(separatedBy: .newlines)
            .map { try! commandInterpreter.interpret(command: $0) }
            .map { try? commandHandler.handle(command: $0) }
        XCTAssertEqual(outputArray.last, "0,0,WEST")
    }
    
    
    /**
         PLACE 1,2,EAST
         MOVE
         MOVE
         LEFT
         MOVE
         REPORT
         Output: 3,3,NORTH
     */

    func testProvidedScenarioC() {
        
        let commands = """
            PLACE 1,2,EAST
            MOVE
            MOVE
            LEFT
            MOVE
            REPORT
            """
        let outputArray = commands.components(separatedBy: .newlines)
            .map { try! commandInterpreter.interpret(command: $0) }
            .map { try? commandHandler.handle(command: $0) }
        XCTAssertEqual(outputArray.last, "3,3,NORTH")
    }
}
