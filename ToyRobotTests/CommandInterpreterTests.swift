//
//  CommandInterpreterTests.swift
//  ToyRobotTests
//
//  Created by JoGoFo on 9/7/19.
//  Copyright © 2019 JOGOFO Pty Ltd. All rights reserved.
//

import XCTest
@testable import ToyRobot

class CommandInterpreterTests: XCTestCase {

    let commandInterpreter = CommandInterpreter()
    
    func testLeftCommand() {
        let leftCommand = try! commandInterpreter.interpret(command: "LEFT")
        guard case Command.left = leftCommand else {
            XCTFail()
            return
        }
    }
    
    func testRightCommand() {
        let rightCommand = try! commandInterpreter.interpret(command: "right")
        guard case Command.right = rightCommand else {
            XCTFail()
            return
        }
    }
    
    func testMoveCommand() {
        let moveCommand = try! commandInterpreter.interpret(command: " Move ")
        guard case Command.move = moveCommand else {
            XCTFail()
            return
        }
    }
    
    func testReportCommand() {
        let reportCommand = try! commandInterpreter.interpret(command: "REPORT x")
        guard case Command.report = reportCommand else {
            XCTFail()
            return
        }
    }
    
    func testPlaceCommand() {
        let placeCommand = try! commandInterpreter.interpret(command: "PLACE 0,1,NORTH")
        guard case let Command.place(position, orientation) = placeCommand else {
            XCTFail()
            return
        }
        XCTAssertEqual(position, Position(x: 0, y: 1))
        XCTAssertEqual(orientation, Orientation.north)
    }
    
    func testPlaceCommand_lowercase() {
        let placeCommand = try! commandInterpreter.interpret(command: "PLACE 5,5,south")
        guard case let Command.place(position, orientation) = placeCommand else {
            XCTFail()
            return
        }
        XCTAssertEqual(position, Position(x: 5, y: 5))
        XCTAssertEqual(orientation, Orientation.south)
    }
    
    func testPlaceCommand_withSpacing() {
        let placeCommand = try! commandInterpreter.interpret(command: "PLACE 0, 1, East ")
        guard case let Command.place(position, orientation) = placeCommand else {
            XCTFail()
            return
        }
        XCTAssertEqual(position, Position(x: 0, y: 1))
        XCTAssertEqual(orientation, Orientation.east)
    }
    
    func testPlaceCommand_invalid() {
        XCTAssertThrowsError(try commandInterpreter.interpret(command: "PLACE 0"))
    }

}
