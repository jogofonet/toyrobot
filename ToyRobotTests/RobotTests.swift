//
//  RobotTests.swift
//  ToyRobotTests
//
//  Created by JoGoFo on 9/7/19.
//  Copyright © 2019 JOGOFO Pty Ltd. All rights reserved.
//

import XCTest
@testable import ToyRobot

class RobotTests: XCTestCase {

    func testRobotTurn() {
        let robot = Robot(position: Position(x: 0, y: 0), orientation: .north)
        XCTAssertEqual(robot.orientation, .north)
        
        robot.turn(.left)
        XCTAssertEqual(robot.orientation, .west)
        
        robot.turn(.right)
        XCTAssertEqual(robot.orientation, .north)
        
        robot.turn(.right)
        robot.turn(.right)
        XCTAssertEqual(robot.orientation, .south)
    }
    
    func testRobotMove() {
        let robot = Robot(position: Position(x: 0, y: 0), orientation: .north)
        let positionalDelegate = StubPositionalDelegate()
        robot.positionalDelegate = positionalDelegate
        
        try! robot.move()
        XCTAssertEqual(robot.position, Position(x: 0, y: 1))
        
        robot.turn(.right)
        try! robot.move()
        XCTAssertEqual(robot.position, Position(x: 1, y: 1))
        
        robot.turn(.right)
        try! robot.move()
        XCTAssertEqual(robot.position, Position(x: 1, y: 0))
        
        robot.turn(.right)
        try! robot.move()
        XCTAssertEqual(robot.position, Position(x: 0, y: 0))
    }

}

// Unit test for a Robot should not be influenced by internal logic implemented by the table, so we'll stub this out
private class StubPositionalDelegate: PositionalDelegate {
    func positionAfterMoving(_ object: MovableObject, spaces: Int, towards orientation: Orientation) throws -> Position {
        let movePositionOffset = orientation.positionalOffset * spaces
        return object.position + movePositionOffset
    }
}
