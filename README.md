# ToyRobot

A robot that can move around a table surface via a command interface.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

## Prerequisites

* Xcode 10 or later


## Building

Open the project file (ToyRobot.xcodeproj) in Xcode and run in the simulator. To install on a physical device a valid development certificate and provisioning profile must be installed and configured in the project as per the [Apple Code Signing guide](https://developer.apple.com/support/code-signing/).

## Running the tests

Unit tests can be run through Xcode.

## Author

* Jonathon Francis
