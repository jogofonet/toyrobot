//
//  TableTopView.swift
//  ToyRobot
//
//  Created by JoGoFo on 10/7/19.
//  Copyright © 2019 JOGOFO Pty Ltd. All rights reserved.
//

import UIKit

fileprivate enum RobotImages {
    static let north = UIImage(named: "robot-north")!
    static let east = UIImage(named: "robot-east")!
    static let south = UIImage(named: "robot-south")!
    static let west = UIImage(named: "robot-west")!
}

class TableTopView: UIView {
    
    private enum Dimensions {
        static let width = 5
        static let height = 5
    }
    
    private static let animationSpeed = 0.3

    @IBOutlet private weak var horizontalStackView: UIStackView!
    
    private var robotViews: [Robot: UIImageView] = [:]
    
    func clear() {
        robotViews.forEach { (_, view) in
            view.removeFromSuperview()
        }
        robotViews.removeAll()
    }
    
    private func view(for position: Position) -> UIView {
        let verticalStackView = horizontalStackView.arrangedSubviews[position.x] as! UIStackView
        return verticalStackView.arrangedSubviews[position.y]
    }
    
    private func robotImage(for orientation: Orientation) -> UIImage {
        switch orientation {
        case .north: return RobotImages.north
        case .east: return RobotImages.east
        case .south: return RobotImages.south
        case .west: return RobotImages.west
        }
    }
    
    /// Translates a position into a position for the local coordinate space. Local coordinate space starts with (0,0) in the top left rather than the bottom left.
    private func translate(position: Position) -> Position {
        return Position(x: position.x, y: Dimensions.height - 1 - position.y)
    }
    
    /// Calculates the view coordinates for a given position. Assumes the Position is in the local coordinate space.
    private func coords(for position: Position) -> CGRect {
        let cellView = view(for: position)
        let rect = cellView.convert(cellView.bounds, to: self)
        return rect
    }
    
    /// Calculates the frame for a robot given a target cell frame
    private func frameForRobot(at rect: CGRect) -> CGRect {
        return rect.inset(by: UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4))
    }
}

extension TableTopView: ToyRobotTableTopView {
    func add(robot: Robot) {
        let translatedPosition = translate(position: robot.position)
        let rect = coords(for: translatedPosition)
        let frame = frameForRobot(at: rect)
        
        let imageView = UIImageView(frame: frame)
        imageView.image = robotImage(for: robot.orientation)
        imageView.contentMode = .scaleAspectFit
        imageView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.addSubview(imageView)
        robotViews[robot] = imageView
    }
    
    func move(robot: Robot) {
        if let imageView = robotViews[robot] {
            imageView.image = robotImage(for: robot.orientation)
            
            let translatedPosition = translate(position: robot.position)
            let rect = coords(for: translatedPosition)
            let frame = frameForRobot(at: rect)
            UIView.animate(withDuration: TableTopView.animationSpeed) {
                imageView.frame = frame
            }
        }
    }
}
