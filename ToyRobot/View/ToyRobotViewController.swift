//
//  ToyRobotViewController.swift
//  ToyRobot
//
//  Created by JoGoFo on 9/7/19.
//  Copyright © 2019 JOGOFO Pty Ltd. All rights reserved.
//

import UIKit

class ToyRobotViewController: UIViewController {
    
    // Presenter should be (and can be) injected, but for simplicity in this single-view application a default value is being initialised here:
    var presenter: ToyRobotPresenter = ToyRobotPresenter()

    @IBOutlet weak var tableTopView: TableTopView!
    @IBOutlet weak var commandsTextField: UITextField!
    @IBOutlet weak var outputTextView: UITextView!
    @IBOutlet var commandsBottomSafeAreaConstraint: NSLayoutConstraint!
    @IBOutlet var commandsBottomConstraint: NSLayoutConstraint!
    
    private let bottomConstraintHeight: CGFloat = 8
    private let defaultAnimationSpeed: Double = 0.25
    
    // MARK:- ViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.outputView = self
        presenter.tableTopView = tableTopView
        commandsTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    
    // MARK: - Keyboard show/hide
    
    @objc func keyboardWillAppear(_ notification: Notification) {
        if let keyboardFrame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double ?? defaultAnimationSpeed
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            UIView.animate(withDuration: duration) {
                self.commandsBottomConstraint.constant = keyboardHeight + self.bottomConstraintHeight
                self.commandsBottomSafeAreaConstraint.isActive = false
                self.commandsBottomConstraint.isActive = true
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func keyboardWillDisappear(_ notification: Notification) {
        let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double ?? defaultAnimationSpeed
        UIView.animate(withDuration: duration) {
            self.commandsBottomConstraint.isActive = false
            self.commandsBottomSafeAreaConstraint.isActive = true
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - User interactions
    
    func completeInput() {
        if let command = commandsTextField.text {
            commandsTextField.text = nil
            presenter.submit(command: command)
        }
    }

}

extension ToyRobotViewController: ToyRobotOutputView {
    func handle(output: String) {
        let fullOutput = outputTextView.text + "\n" + output
        outputTextView.text = fullOutput
        outputTextView.scrollRangeToVisible(NSRange(location: fullOutput.count - 2, length: 1))
    }
}

extension ToyRobotViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        completeInput()
        return false
    }
}

