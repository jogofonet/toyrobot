//
//  ToyRobotPresenter.swift
//  ToyRobot
//
//  Created by JoGoFo on 9/7/19.
//  Copyright © 2019 JOGOFO Pty Ltd. All rights reserved.
//

import Foundation

protocol ToyRobotOutputView: AnyObject {
    func handle(output: String)
}

protocol ToyRobotTableTopView: AnyObject {
    func add(robot: Robot)
    func move(robot: Robot)
}

class ToyRobotPresenter {
    
    weak var outputView: ToyRobotOutputView?
    weak var tableTopView: ToyRobotTableTopView?
    
    private let commandInterpreter = CommandInterpreter()
    private let coordinator = ToyRobotCoordinator()
    
    init() {
        coordinator.delegate = self
    }
    
    func submit(command: String) {
        outputView?.handle(output: command)
        do {
            let commandHandler = CommandHandler(coordinator: coordinator)
            let commandEnum = try commandInterpreter.interpret(command: command)
            if let output = try commandHandler.handle(command: commandEnum) {
                outputView?.handle(output: output)
            }
        } catch is CommandInterpreter.Errors {
            outputView?.handle(output: "Invalid command")
        } catch {
            // There is no requirement to do anything with discarded commands e.g. attempting to move out of bounds, but any specific scenario could be handled here
        }
    }
}

extension ToyRobotPresenter: ToyRobotCoordinatorDelegate {
    func created(robot: Robot) {
        tableTopView?.add(robot: robot)
    }
    
    func robotMoved(_ robot: Robot) {
        tableTopView?.move(robot: robot)
    }
}
