//
//  Orientation.swift
//  ToyRobot
//
//  Created by JoGoFo on 9/7/19.
//  Copyright © 2019 JOGOFO Pty Ltd. All rights reserved.
//

import Foundation

enum Orientation: String {
    case north = "NORTH"
    case east = "EAST"
    case south = "SOUTH"
    case west = "WEST"
}

extension Orientation {
    
    func turn(to direction: Direction) -> Orientation {
        switch direction {
        case .right:
            switch self {
            case .north: return .east
            case .east: return .south
            case .south: return .west
            case .west: return .north
            }
            
        case .left:
            switch self {
            case .north: return .west
            case .west: return .south
            case .south: return .east
            case .east: return .north
            }
        }
    }
    
    var positionalOffset: Position {
        switch self {
        case .north: return Position(x: 0, y: 1)
        case .east: return Position(x: 1, y: 0)
        case .south: return Position(x: 0, y: -1)
        case .west: return Position(x: -1, y: 0)
        }
    }
    
}
