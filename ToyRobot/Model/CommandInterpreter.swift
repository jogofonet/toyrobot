//
//  CommandInterpreter.swift
//  ToyRobot
//
//  Created by JoGoFo on 9/7/19.
//  Copyright © 2019 JOGOFO Pty Ltd. All rights reserved.
//

import Foundation

/// Interperets text commands into a known command (enum) with parameters if relevant
struct CommandInterpreter {
    
    enum Errors: Error {
        case invalidCommand
        case invalidParameters
    }
    
    /// Interperets a command string into a known command
    ///
    /// - Parameters:
    ///     - command: The text command to be interpreted
    ///
    /// - Returns: An enum representation of the command
    ///
    /// - Throws: `CommandInterpreter.Errors` when the command or parameters could not be processed
    func interpret(command: String) throws -> Command {
        let cleanCommand = command.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        let components = cleanCommand.components(separatedBy: .whitespaces)
        
        guard let firstComponent = components.first else {
            throw Errors.invalidCommand
        }
        
        let parameters = cleanCommand.dropFirst(firstComponent.count).trimmingCharacters(in: .whitespaces)
        
        switch firstComponent {
        case "PLACE":
            return try parsePlace(parameterString: parameters)
        case "LEFT":
            return .left
        case "RIGHT":
            return .right
        case "MOVE":
            return .move
        case "REPORT":
            return .report
        default:
            throw Errors.invalidCommand
        }
    }
    
    private func parsePlace(parameterString: String) throws -> Command {
        let parameters = parameterString.components(separatedBy: ",")
            .map { return $0.trimmingCharacters(in: .whitespaces) }
        guard parameters.count >= 3,
            let x = Int(parameters[0]),
            let y = Int(parameters[1]) else {
                throw Errors.invalidParameters
        }
        let position = Position(x: x, y: y)
        
        guard let orientation = Orientation(rawValue: parameters[2]) else {
            throw Errors.invalidParameters
        }
        
        return .place(position, orientation)
    }
}
