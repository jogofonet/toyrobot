//
//  ToyRobotCoordinator.swift
//  ToyRobot
//
//  Created by JoGoFo on 10/7/19.
//  Copyright © 2019 JOGOFO Pty Ltd. All rights reserved.
//

import Foundation

protocol ToyRobotCoordinatorDelegate: AnyObject {
    func created(robot: Robot)
    func robotMoved(_ robot: Robot)
}

/// Maintains state of a `Table` and exposes actions that can be performed. Notifies the `delegate` of any state changes
class ToyRobotCoordinator {
    
    weak var delegate: ToyRobotCoordinatorDelegate?
    
    let table: Table
    private(set) var currentRobot: Robot?
    
    init(table: Table = Table(width: 5, length: 5)) {
        self.table = table
    }
    
    
    func place(at position: Position, facing orientation: Orientation) throws {
        let robot = Robot(position: position, orientation: orientation)
        try table.place(robot, position: position)
        robot.positionalDelegate = table
        currentRobot = robot
        delegate?.created(robot: robot)
    }
    
    func left() {
        if let robot = currentRobot {
            robot.turn(.left)
            delegate?.robotMoved(robot)
        }
    }
    
    func right() {
        if let robot = currentRobot {
            robot.turn(.right)
            delegate?.robotMoved(robot)
        }
    }
    
    func move() throws {
        if let robot = currentRobot {
            try robot.move()
            delegate?.robotMoved(robot)
        }
    }
}
