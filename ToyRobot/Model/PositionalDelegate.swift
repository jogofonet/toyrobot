//
//  PositionalDelegate.swift
//  ToyRobot
//
//  Created by JoGoFo on 9/7/19.
//  Copyright © 2019 JOGOFO Pty Ltd. All rights reserved.
//

import Foundation

/// An abstract inferface is used again as a robot (or other object) could be placed on any type of surface
protocol PositionalDelegate: AnyObject {
    func positionAfterMoving(_ object: MovableObject, spaces: Int, towards orientation: Orientation) throws -> Position
}

enum PositionError: Error {
    case notPlaced
    case outOfBounds
}
