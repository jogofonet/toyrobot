//
//  Table.swift
//  ToyRobot
//
//  Created by JoGoFo on 9/7/19.
//  Copyright © 2019 JOGOFO Pty Ltd. All rights reserved.
//

import Foundation

/// The table on which the robots can move around
class Table: PositionalDelegate {
    
    let width: Int
    let length: Int
    
    private(set) var objects: [MovableObject] = []
    
    init(width: Int, length: Int) {
        self.width = width
        self.length = length
    }
    
    /// Places an object on the table if specified position is valid. The orientation of the object is not relevant to the Table.
    ///
    /// - Note: Once successfully added to the table, the *MovableObject* should set its *PositionalDelegate* to the table so that it can be spacially aware of its surroundings.
    func place(_ tableObject: MovableObject, position: Position) throws {
        guard !outOfBounds(position) else {
            throw PositionError.outOfBounds
        }
        objects.append(tableObject)
    }
    
    /// Calculates the new position on the table after moving *spaces* towards the *orientation* specified, if valid. The table is not responsible for actually moving the object, the object should move itself.
    func positionAfterMoving(_ object: MovableObject, spaces: Int, towards orientation: Orientation) throws -> Position {
        guard objects.contains(where: { movableObject -> Bool in
            movableObject === object
        }) else {
            throw PositionError.notPlaced
        }
        
        let movePositionOffset = orientation.positionalOffset * spaces
        let newPosition = object.position + movePositionOffset
        
        if outOfBounds(newPosition) {
            throw PositionError.outOfBounds
        }
        
        return newPosition
    }
    
    private func outOfBounds(_ position: Position) -> Bool {
        return position.x >= width || position.y >= length || position.x < 0 || position.y < 0
    }
    
}


/// A abstract interface for table objects is used to eliminate the assumption that there will always only
/// ever be Robots that can move about the table.
protocol MovableObject: AnyObject {
    var position: Position { get }
}
