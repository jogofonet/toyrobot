//
//  Position.swift
//  ToyRobot
//
//  Created by JoGoFo on 9/7/19.
//  Copyright © 2019 JOGOFO Pty Ltd. All rights reserved.
//

import Foundation

struct Position: Equatable {
    let x: Int
    let y: Int
    
    static let invalidPosition = Position(x: Int.max, y: Int.max)
}

extension Position {
    static func + (lhs: Position, rhs: Position) -> Position {
        return Position(x: lhs.x + rhs.x, y: lhs.y + rhs.y)
    }
    
    static func * (lhs: Position, rhs: Int) -> Position {
        return Position(x: lhs.x * rhs, y: lhs.y * rhs)
    }
}
