//
//  Robot.swift
//  ToyRobot
//
//  Created by JoGoFo on 9/7/19.
//  Copyright © 2019 JOGOFO Pty Ltd. All rights reserved.
//

import Foundation

class Robot: MovableObject {
    
    private var id = UUID()
    
    private(set) var orientation: Orientation
    private(set) var position: Position
    
    weak var positionalDelegate: PositionalDelegate?
    
    static let moveDistance = 1
    
    init(position: Position, orientation: Orientation) {
        self.orientation = orientation
        self.position = position
    }
    
    func turn(_ direction: Direction) {
        let newOrientation = orientation.turn(to: direction)
        orientation = newOrientation
    }
    
    func move() throws {
        guard let positionalDelegate = positionalDelegate else {
            return
        }
        
        // A robot moves itself, it doesn't ask the table to move it
        let newPosition = try positionalDelegate.positionAfterMoving(self, spaces: Robot.moveDistance, towards: orientation)
        position = newPosition
    }
}

extension Robot: Hashable {
    static func == (lhs: Robot, rhs: Robot) -> Bool {
        return lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
