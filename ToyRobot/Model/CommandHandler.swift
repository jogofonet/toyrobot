//
//  CommandHandler.swift
//  ToyRobot
//
//  Created by JoGoFo on 10/7/19.
//  Copyright © 2019 JOGOFO Pty Ltd. All rights reserved.
//

import Foundation

enum Command {
    case place(Position, Orientation)
    case left
    case right
    case move
    case report
}

/// Handles commands by executing them on the provided 'ToyRobotCoordinator'
struct CommandHandler {

    let coordinator: ToyRobotCoordinator
    
    init(coordinator: ToyRobotCoordinator) {
        self.coordinator = coordinator
    }
    
    /// Maps an enum representation of a command to a method and *executes* it on the provided `ToyRobotCoordinator`, returning any relevant output.
    ///
    /// - Parameters:
    ///     - command: Enum representation of the command to be executed
    ///
    /// - Returns: Output of the command, if any
    ///
    /// - Throws: When executing the command was unsuccessful
    func handle(command: Command) throws -> String? {
        switch command {
        case let .place(position, orientation):
            try coordinator.place(at: position, facing: orientation)
        case .left:
            coordinator.left()
        case .right:
            coordinator.right()
        case .move:
            try coordinator.move()
        case .report:
            return report()
        }
        return nil
    }
    
    private func report() -> String? {
        guard let robot = coordinator.currentRobot else {
            return nil
        }
        return "\(robot.position.x),\(robot.position.y),\(robot.orientation.rawValue)"
    }
}
